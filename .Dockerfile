FROM multiphenics/multiphenics
MAINTAINER Francesco Ballarin <francesco.ballarin@sissa.it>

USER fenics
WORKDIR $FENICS_HOME
COPY --chown=fenics . CFD-course-examples/

USER root
