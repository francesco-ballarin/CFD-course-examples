This repository contains a few tutorials developed by [Dr. Francesco Ballarin](http://people.sissa.it/~fballarin/) for an introductory course on computational fluid dynamics (lecturer: [Prof. Gianluigi Rozza](http://people.sissa.it/~grozza/)) for the PhD program on "Mathematical Analysis, Modelling, and Applications" at SISSA, Trieste, Italy.

### How to run on Google Colab
You can [run the tutorials on Google Colab](https://colab.research.google.com/drive/1O4qyVzP7ZbrQwLsds139P7G5JT3-npER?usp=sharing) by saving a copy to your Google Drive (`File -> Save a copy in Drive`).

### How to use docker image
##### Setup (only once)
1. Install the FEniCS Docker script as explained in [the "Quickstart" section on the official FEniCS Containers documentation](http://fenics.readthedocs.io/projects/containers/en/latest/quickstart.html#quickstart)

2. Create a new container by typing
```
fenicsproject notebook cfd-course-examples cfdcourse/examples
```
See [the "Running Jupyter notebooks" section on the official FEniCS Containers documentation](http://fenics.readthedocs.io/projects/containers/en/latest/jupyter.html) for further details.

##### Start
1. Run the container as follows:
```
fenicsproject start cfd-course-examples
```
The resulting output should contain the following lines
```
You can access the Jupyter notebook at ADDRESS
[...]
    Copy/paste this URL into your browser when you connect for the first time,
    to login with a token:
        http://IGNORE_THIS/?token=TOKEN
```
The provided Jupyter session will run at
```
http://ADDRESS/?token=TOKEN
```

2. Jupyter notebooks are available at
```
CFD-course-examples
```

3. You can open a terminal on the running container by typing
```
docker exec -i -t --user fenics cfd-course-examples /bin/bash
```
